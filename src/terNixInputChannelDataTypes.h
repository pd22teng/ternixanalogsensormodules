/*
 * terNixInputChannelDataTypes.h
 *
 *  Created on: Sep 4, 2015
 *      Author: teng
 */

#ifndef TERNIXINPUTCHANNELDATATYPES_H_
#define TERNIXINPUTCHANNELDATATYPES_H_

#include "glib.h"
#include "terNixI2cTask.h"
#include "terNixGeneralDataTypes.h"

typedef enum {
	TERNIX_INPUT_NULL = 0,
	TERNIX_INPUT_ID_1,
	TERNIX_INPUT_ID_2,
	TERNIX_INPUT_ID_3,
	TERNIX_INPUT_ID_4,
	TERNIX_INPUT_ID_5,
	TERNIX_INPUT_ID_6,
	TERNIX_INPUT_ID_7,
	TERNIX_INPUT_ID_8,
	TERNIX_INPUT_ID_9,
	TERNIX_INPUT_ID_10,
	TERNIX_INPUT_ID_11,
	TERNIX_INPUT_ID_12,
	TERNIX_INPUT_ID_13,
	TERNIX_INPUT_ID_14,
	TERNIX_INPUT_ID_15,
	TERNIX_INPUT_ID_16,
	TERNIX_INPUT_ID_17,
	TERNIX_INPUT_ID_18,
	TERNIX_INPUT_ID_19,
	TERNIX_INPUT_ID_20,
	TERNIX_INPUT_ID_21,
	TERNIX_INPUT_ID_22,
	TERNIX_INPUT_ID_23,
	TERNIX_INPUT_ID_24,
	TERNIX_INPUT_ID_25,
	TERNIX_INPUT_ID_26,
	TERNIX_INPUT_ID_27,
	TERNIX_INPUT_ID_28,
	TERNIX_INPUT_ID_29,
	TERNIX_INPUT_ID_30,
	TERNIX_INPUT_ID_31,
	TERNIX_INPUT_ID_32,
	TERNIX_INPUT_ID_MAX = 32
} TernixInputID;

#define TERNIX_INPUTS_NUMBER	32

#define TERNIX_DISABLE_INPUT 0
#define TERNIX_ENABLE_INPUT  1

#define TERNIX_INPUT_ENABLED    0x01

#define TERNIX_DIGITAL_INPUT  0
#define TERNIX_ANALOG_INPUT   1

#define TERNIX_SET_INPUT_ANALOG     0x02

#define TERNIX_CURRENT_LOOP_MODE  0
#define TERNIX_VOLTAGE_MODE       1

#define TERNIX_SET_INPUT_VOLTAGE_MODE   0x04

#define TERNIX_INPUT_DEF_THRESHOLD      0x5

#define TERNIX_INPUT_DEFAULT_THRESHOLD    0x28

typedef union {
    guchar val;
    struct {
        guchar enable           : 1;   // Enable / Disable
        guchar mode             : 1;   // Analog / Digital
        guchar c_or_v           : 1;   // Input single: Current loop / Voltage
        guchar threshold        : 4;   // Threshold value for triggering value change interrupt
        guchar reserved         : 1;
    } bits;
} TernixInputControlRegisterByte;

typedef struct {
	TernixInputControlRegisterByte RegByte;
	guchar conRegI2CWriteBuff;
	gboolean isConModified;
	GMutex conRegMutex;
	GCond conRegCond;
	GMutex changeMutex;
	I2CTaskCallBack callback;
	gboolean isConfigSucceed;
	GThreadFunc conWriteTask;
} TernixAnalogSensorControlRegister;

typedef struct {
	uint16 analogValue;
	uint16_val analogValueI2CReadBuff;
	GMutex valRegMutex;
	GMutex updateMutex;
	GCond updateCond;
	I2CTaskCallBack callback;
	gboolean isReadSucceed;
	gboolean isSensorDataUpdated;
	GThreadFunc valReadTask;
} TernixAnalogSensorValueRegister;

typedef struct {
	TernixInputID id;
	guchar moduleID;
	TernixAnalogSensorControlRegister conRegVariables;
	TernixAnalogSensorValueRegister valRegVariables;
} TernixAnalogSensorChannel;

#endif /* TERNIXINPUTCHANNELDATATYPES_H_ */
