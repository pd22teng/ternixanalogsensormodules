/*
 * terNixAnalogModulesTable.h
 *
 *  Created on: Nov 6, 2015
 *      Author: teng
 */

#ifndef TERNIXANALOGMODULESTABLE_H_
#define TERNIXANALOGMODULESTABLE_H_

#include "terNixAnalogSensorModuleDataTypes.h"

extern const TernixAnalogModuleInfoItem terNixAnalogModuleInfoTable[TERNIX_ANALOG_MODULE_NUM];

guchar getChannelIndexFromChannelID(guchar channelId);
guchar getModuleIDFromChannelID(guchar channelId);
guchar getModuleIDFromI2cAddress(guchar i2cAddress);

#endif /* TERNIXANALOGMODULESTABLE_H_ */
