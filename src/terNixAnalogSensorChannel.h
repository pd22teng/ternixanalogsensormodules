/*
 * terNixAnalogSensorChannel.h
 *
 *  Created on: Sep 4, 2015
 *      Author: teng
 */

#ifndef TERNIXANALOGSENSORCHANNEL_H_
#define TERNIXANALOGSENSORCHANNEL_H_

#include "terNixI2cTask.h"
#include "terNixInputChannelDataTypes.h"

void initChannel(TernixAnalogSensorChannel * channel);

void enableChannel(TernixAnalogSensorChannel * channel);
void disableChannel(TernixAnalogSensorChannel * channel);
gboolean isChannelEnabled(TernixAnalogSensorChannel * channel);

void startChannelTasks(TernixAnalogSensorChannel * channel);

void setSignalModeToCurrent(TernixAnalogSensorChannel * channel);
void setSignalModeToVoltage(TernixAnalogSensorChannel * channel);

void setThreshold(TernixAnalogSensorChannel * channel, guchar val);

void setChannelConReg(TernixAnalogSensorChannel * channel, guchar val);

guchar getChannelConRegVal(TernixAnalogSensorChannel * channel);

void setChannelValRegUpdateFlag(TernixAnalogSensorChannel * channel, gboolean flag);

uint16 getChannelAnalogValue(TernixAnalogSensorChannel * channel);

ErrorCode I2cReadChannelVal(TernixAnalogSensorChannel * channel);
ErrorCode I2cUpdateChannelConfig(TernixAnalogSensorChannel * channel);

#endif /* TERNIXANALOGSENSORCHANNEL_H_ */
