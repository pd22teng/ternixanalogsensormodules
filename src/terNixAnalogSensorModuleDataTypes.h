/*
 * terNixAnalogSensorModuleDataTypes.h
 *
 *  Created on: Sep 4, 2015
 *      Author: teng
 */

#ifndef TERNIXANALOGSENSORMODULEDATATYPES_H_
#define TERNIXANALOGSENSORMODULEDATATYPES_H_

#include "terNixInputChannelDataTypes.h"

#define TERNIX_ANALOG_MODULE_NUM	6

typedef enum {
	TERNIX_ANALOG_MODULE_NULL = 0,
	TERNIX_ANALOG_MODULE_ONE,
	TERNIX_ANALOG_MODULE_TWO,
	TERNIX_ANALOG_MODULE_THR,
	TERNIX_ANALOG_MODULE_FOU,
	TERNIX_ANALOG_MODULE_FIV,
	TERNIX_ANALOG_MODULE_SIX,
	TERNIX_ANALOG_MODULE_MAX = 6
} TernixAnalogModuleID;

#define TERNIX_SNESOR_MODULE_ONE_CH_NUM 6
#define TERNIX_SNESOR_MODULE_TWO_CH_NUM 5
#define TERNIX_SNESOR_MODULE_THR_CH_NUM 6
#define TERNIX_SNESOR_MODULE_FOU_CH_NUM 5
#define TERNIX_SNESOR_MODULE_FIV_CH_NUM 5
#define TERNIX_SNESOR_MODULE_SIX_CH_NUM 5

typedef struct {
	TernixAnalogModuleID id;
	guchar i2cAddress;
	guchar channelNum;
	TernixInputID * inputChannels;
} TernixAnalogModuleInfoItem;

typedef struct {
	uint8_val channlesUpdate;
	union {
		guchar val;
		struct {
			guchar initialised : 1;
			guchar connected : 1;
			guchar reserved: 6;
		} bits;
	} flags;
} TernixAnalogModuleStatusRegister;

typedef struct {
	const TernixAnalogModuleInfoItem * info;
	TernixAnalogModuleStatusRegister status;
	guchar channelsInitConfigRegBuff[6];
	I2CTaskCallBack callback;
} TernixAnalogModule;

#endif /* TERNIXANALOGSENSORMODULEDATATYPES_H_ */
