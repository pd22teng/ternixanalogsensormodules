/*
 * Copyright (C) Prodevice Oy Copyright.
 *
 * Author: Teng Wu
 * 
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "terNixAnalogSensorChannel.h"
#include "terNixAnalogModulesTable.h"
#include "GPIOFunctions.h"

#define TERNIX_SENSOR_MODULE_I2C_BUS 		I2C_BUS_1
#define TERNIX_ANALOG_INPUT_CHANGED_INT 	AT91_PIN_PC31

TernixAnalogModule _modules[TERNIX_ANALOG_MODULE_NUM];

TernixAnalogSensorChannel _channels[TERNIX_INPUTS_NUMBER];

static void analogModuleI2cTaskCallBack(guchar i2cAddress, guchar i2cCMD, gpointer para, gint result);

static void initTernixAnalogChannels(void);
static void initTernixAnalogModules(void);

static void initSingalHandler(void);
static void quit_task(int signal);

static void initAnalogModuleInterrupt(void);
static gboolean AnalogModuleInterruptEventHandler(GIOChannel *channel, GIOCondition condition, gpointer user_data);

int main(void)
{
	// Process quit and terminated
	initSingalHandler();

	// Initialize I2C driver and Task
	initI2cTask(TERNIX_SENSOR_MODULE_I2C_BUS);

	// start I2C Task
	startI2cTask();

	initTernixAnalogChannels();
	initTernixAnalogModules();

	initAnalogModuleInterrupt();

	GMainLoop* GpioMainLoop = g_main_loop_new(0, 0);

	// listen to the CMD and Config
//	g_thread_new("User Interface thread", NULL, NULL);

	g_main_loop_run(GpioMainLoop);

	return 0;
}

void initTernixAnalogChannels(void)
{
	guchar i;

	TernixAnalogSensorChannel * channel = NULL;

	for(i = 0; i < TERNIX_INPUTS_NUMBER; i++)
	{
		channel = &_channels[i];
		channel->id = i + 1;

		initChannel(channel);
	}
}

void initTernixAnalogModules(void)
{
	guchar i;

	TernixAnalogModule * mod = NULL;

	for(i = 0; i < TERNIX_ANALOG_MODULE_NUM; i++)
	{
		mod = &_modules[i];

		mod->info = &terNixAnalogModuleInfoTable[i];
		mod->callback = analogModuleI2cTaskCallBack;
		mod->status.channlesUpdate.val = 0;
		mod->status.flags.val = 0;

		isI2cDeviceReadyForSession(mod->info->i2cAddress, mod->callback);
	}
}

ErrorCode queryUpdateStatus(guchar moduleID)
{
	TernixAnalogModule * mod = &_modules[moduleID - 1];
	guchar i2cAddress = mod->info->i2cAddress;

    return addNewI2cSession(mod->callback, NULL, i2cAddress, CMD_ANALOG_MODULE_UPATE_STATE_QUERY, I2C_TERNIX_NULL_MESSAGE_LEN, I2C_TERNIX_ANALOG_MODULE_UPDATE_STATUS_MSG_LEN, NULL, &mod->status.channlesUpdate.val);
}

ErrorCode i2cModuleInitConfig(guchar moduleID)
{
	guchar i;
	guchar channelID;

	TernixAnalogModule * mod = &_modules[moduleID - 1];
	guchar i2cAddress = mod->info->i2cAddress;
	guchar chNum = mod->info->channelNum;
	TernixInputID * channels = mod->info->inputChannels;

	for(i = 0 ; i < chNum; i++) {
		channelID = channels[i];
		mod->channelsInitConfigRegBuff[i] = getChannelConRegVal(&_channels[channelID]);
	}

	for(i = chNum - 1; i < 6; i++)
		mod->channelsInitConfigRegBuff[i] = 0;

	return addNewI2cSession(mod->callback, NULL, i2cAddress, CMD_ANALOG_MODULE_INIT_CONFIG, chNum * I2C_TERNIX_PROPO_MODULE_INIT_CONFIG_MSG_LEN, I2C_TERNIX_NULL_MESSAGE_LEN, &mod->channelsInitConfigRegBuff[0], NULL);
}

void analogModuleI2cTaskCallBack(guchar i2cAddress, guchar i2cCMD, gpointer para, gint result)
{
	guchar modId = 0;
	guchar i;
	TernixInputID * chs;
	guchar chNum;

	modId = getModuleIDFromI2cAddress(i2cAddress);
	TernixAnalogModule * mod = &_modules[modId - 1];

	switch(i2cCMD) {
	case CMD_NULL:
		if(result != Success) {
			mod->status.flags.bits.connected = NO;
			printf("Module %d(i2cAddress: %d) is disconnected\n", modId, i2cAddress);

			chNum = mod->info->channelNum;
			chs = mod->info->inputChannels;
			for(i = 0; i < chNum; i++)
			{
				disableChannel(&_channels[chs[i] - 1]);
			}
		}
		else {
			if(!mod->status.flags.bits.connected) {
				mod->status.flags.bits.connected = YES;

				chNum = mod->info->channelNum;
				chs = mod->info->inputChannels;
				for(i = 0; i < chNum; i++)
				{
					startChannelTasks(&_channels[chs[i] - 1]);
				}

				i2cModuleInitConfig(mod->info->id);
				printf("Module %d(i2cAddress: %d) is connected\n", modId, i2cAddress);
			}
		}
		break;
	case CMD_ANALOG_MODULE_INIT_CONFIG:
		if(result != Success)
			mod->status.flags.bits.initialised = NO;
		else {
			mod->status.flags.bits.initialised = YES;

			printf("Module %d(i2cAddress: %d) is initialized\n", modId, i2cAddress);
		}
		break;
	case CMD_ANALOG_MODULE_UPATE_STATE_QUERY:
		if(result == Success) {
			chNum = mod->info->channelNum;
			chs = mod->info->inputChannels;

			printf("Module %d(i2cAddress: %d) queried update state: %d \n", modId, i2cAddress, mod->status.channlesUpdate.val);

			for(i = 0; i < chNum; i++)
			{
				if(mod->status.channlesUpdate.val & (1 << i))
					setChannelValRegUpdateFlag(&_channels[chs[i] - 1], TRUE);
			}
		}
		break;
	default: break;
	}
}

static void initAnalogModuleInterrupt(void)
{
	if(gpio_set_input_mode(TERNIX_ANALOG_INPUT_CHANGED_INT) >= 0) {
		gpio_set_glib_event_handler(TERNIX_ANALOG_INPUT_CHANGED_INT, AnalogModuleInterruptEventHandler, (gpointer)NULL);

	    gpio_set_edge(TERNIX_ANALOG_INPUT_CHANGED_INT, RISING_EDGE);
	}
}

static gboolean AnalogModuleInterruptEventHandler(GIOChannel *channel, GIOCondition condition, gpointer user_data)
{
    guchar i = 0;

    guchar digit = LOW;
    gchar buf[2];
	gsize byte_read = 0;
	GError *error = 0;
    TernixAnalogModule * analog = NULL;

	g_io_channel_seek_position(channel, 0, G_SEEK_SET, 0);
	g_io_channel_read_chars(channel, buf, 1, &byte_read, &error);

	if(buf[0] == '1')
		digit = HIGH;
	else
		return TRUE;

    if(digit == HIGH) {
    	printf("Modules Interrupt triggered\n");
    	for(i = 0; i < TERNIX_ANALOG_MODULE_NUM; i++) {
    		analog = &_modules[i];
    		if(analog->status.flags.bits.initialised) {
    			if(queryUpdateStatus(analog->info->id) != Success)
					printf("query update Message send failed\n");
				else
					printf("query update Message sent\n");
    		}
    	}
    }

    return TRUE;
}

static void initSingalHandler(void)
{
	struct sigaction sig_term_action;
	memset(&sig_term_action, 0, sizeof(sig_term_action));
	sig_term_action.sa_handler = quit_task;

	sigaction(SIGQUIT, &sig_term_action, NULL);
	sigaction(SIGTERM, &sig_term_action, NULL);
}

static void quit_task(int signal)
{
	stopI2cTask();
}
