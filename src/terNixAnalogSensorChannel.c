/*
 * terNixAnalogSensorChannel.c
 *
 *  Created on: Sep 4, 2015
 *      Author: teng
 */

#include <stdio.h>
#include "terNixAnalogSensorChannel.h"
#include "terNixAnalogModulesTable.h"

static void initChannelConReg(TernixAnalogSensorControlRegister * conReg);
static void initChannelValReg(TernixAnalogSensorValueRegister * valReg);

static gpointer channelConRegMonitorTask(gpointer data);
static gpointer channelValRegSychornizeTask(gpointer data);

static void channelConRegI2cTaskCallBack(guchar i2cAddress, guchar i2cCMD, gpointer para, gint result);
static void channelValRegI2cTaskCallBack(guchar i2cAddress, guchar i2cCMD, gpointer para, gint result);

static void refreshUpdatedValReg(TernixAnalogSensorChannel * channel);
static void setChannelConRegUpdateFlag(TernixAnalogSensorControlRegister * conReg, gboolean flag);

static void setChannelConRegBit(TernixAnalogSensorChannel * channel, guchar val);
static void clearChannelConRegBit(TernixAnalogSensorChannel * channel, guchar val);

static gboolean isChannelConRegModified(TernixAnalogSensorControlRegister* conReg);
static gboolean isChannelConRegUpdateSucceed(TernixAnalogSensorControlRegister* conReg);
static void clearChannelConRegModified(TernixAnalogSensorControlRegister* conReg);

void initChannel(TernixAnalogSensorChannel * channel)
{
	// Parent module ID
	channel->moduleID = getModuleIDFromChannelID(channel->id);

	// initialize control register
	initChannelConReg(&channel->conRegVariables);
	// initialize value register
	initChannelValReg(&channel->valRegVariables);
}

// CONTROL REGISTER
static void initChannelConReg(TernixAnalogSensorControlRegister * conReg)
{
	conReg->RegByte.val = TERNIX_INPUT_ENABLED | TERNIX_SET_INPUT_ANALOG | TERNIX_INPUT_DEFAULT_THRESHOLD;

	conReg->isConModified = FALSE;
	conReg->isConfigSucceed = TRUE;

	conReg->conWriteTask = channelConRegMonitorTask;
	conReg->callback = channelConRegI2cTaskCallBack;

	g_mutex_init(&conReg->changeMutex);
	g_cond_init(&conReg->conRegCond);
	g_mutex_init(&conReg->conRegMutex);
}

void setChannelConReg(TernixAnalogSensorChannel * channel, guchar val)
{
	TernixAnalogSensorControlRegister * conReg = &channel->conRegVariables;

	g_mutex_lock(&conReg->conRegMutex);
	if(conReg->RegByte.val != val) {
		conReg->RegByte.val = val;
		conReg->conRegI2CWriteBuff = val;
		setChannelConRegUpdateFlag(conReg, TRUE);
		g_cond_signal(&conReg->conRegCond);

	}
	g_mutex_unlock(&conReg->conRegMutex);
}

guchar getChannelConRegVal(TernixAnalogSensorChannel * channel)
{
	guchar val;
	TernixAnalogSensorControlRegister * conReg = &channel->conRegVariables;

	g_mutex_lock(&conReg->conRegMutex);
	val = conReg->RegByte.val;
	g_mutex_unlock(&conReg->conRegMutex);

	return val;
}

void setChannelConRegUpdateFlag(TernixAnalogSensorControlRegister * conReg, gboolean flag)
{
	g_mutex_lock(&conReg->changeMutex);
	conReg->isConModified = flag;
	conReg->isConfigSucceed = ~flag;
	g_mutex_unlock(&conReg->changeMutex);
}

static gboolean isChannelConRegModified(TernixAnalogSensorControlRegister* conReg)
{
	gboolean isModified = FALSE;

	g_mutex_lock(&conReg->changeMutex);
	isModified = conReg->isConModified;
	g_mutex_unlock(&conReg->changeMutex);

	return isModified;
}

static void clearChannelConRegModified(TernixAnalogSensorControlRegister* conReg)
{
	g_mutex_lock(&conReg->changeMutex);
	conReg->isConModified = FALSE;
	g_mutex_unlock(&conReg->changeMutex);
}

static gboolean isChannelConRegUpdateSucceed(TernixAnalogSensorControlRegister* conReg)
{
	gboolean isSucceed = FALSE;

	g_mutex_lock(&conReg->changeMutex);
	isSucceed = conReg->isConfigSucceed;
	g_mutex_unlock(&conReg->changeMutex);

	return isSucceed;
}

gpointer channelConRegMonitorTask(gpointer data)
{
	TernixAnalogSensorChannel * channel = (TernixAnalogSensorChannel*)data;
	TernixAnalogSensorControlRegister* conReg = &channel->conRegVariables;

	printf("Channel: %d sensor configuration update task start\n", ((TernixAnalogSensorChannel*)data)->id);

	while(isChannelEnabled(channel)) {
		//	lockConRegMutex
		g_mutex_lock(&conReg->conRegMutex);
		while(isChannelConRegUpdateSucceed(conReg) && !isChannelConRegModified(conReg))
			g_cond_wait(&conReg->conRegCond, &conReg->conRegMutex);
		g_mutex_unlock(&conReg->conRegMutex);

		clearChannelConRegModified(conReg);

		if(I2cUpdateChannelConfig((TernixAnalogSensorChannel*)data) != Success)
			printf("I2C add Session failed\n");

		g_thread_yield();
	}
	printf("Channel: %d sensor configuration update task end\n", ((TernixAnalogSensorChannel*)data)->id);

	g_thread_exit(data);

	return NULL;
}

void channelConRegI2cTaskCallBack(guchar i2cAddress, guchar i2cCMD, gpointer para, gint result)
{
	TernixAnalogSensorChannel * channel = (TernixAnalogSensorChannel *)para;

	switch(i2cCMD) {
	case CMD_ANALOG_CHANNEL_CONFIG:
		if(result == Success) {
			setChannelConRegUpdateFlag(&channel->conRegVariables, FALSE);
			printf("Channel %d is reconfigured\n", channel->id);
		}
		break;
	default: break;
	}
}

//VALUE REGISTER
static void initChannelValReg(TernixAnalogSensorValueRegister * valReg)
{
	valReg->analogValue = 0;
	valReg->isSensorDataUpdated = FALSE;
	valReg->isReadSucceed = TRUE;
	valReg->valReadTask = channelValRegSychornizeTask;
	valReg->callback = channelValRegI2cTaskCallBack;

	g_mutex_init(&valReg->updateMutex);
	g_cond_init(&valReg->updateCond);
	g_mutex_init(&valReg->valRegMutex);
}

void setChannelValRegUpdateFlag(TernixAnalogSensorChannel * channel, gboolean flag)
{
	TernixAnalogSensorValueRegister * valReg = &channel->valRegVariables;

	g_mutex_lock(&valReg->updateMutex);
	valReg->isSensorDataUpdated = flag;
	valReg->isReadSucceed = ~flag;
	g_mutex_unlock(&valReg->updateMutex);

	if(flag) g_cond_signal(&valReg->updateCond);
}

uint16 getChannelAnalogValue(TernixAnalogSensorChannel * channel)
{
	uint16 val;
	TernixAnalogSensorValueRegister * valReg = &channel->valRegVariables;

	g_mutex_lock(&valReg->valRegMutex);
	val = valReg->analogValue;
	g_mutex_unlock(&valReg->valRegMutex);

	return val;
}

static void refreshUpdatedValReg(TernixAnalogSensorChannel * channel)
{
	TernixAnalogSensorValueRegister * valReg = &channel->valRegVariables;

	g_mutex_lock(&valReg->valRegMutex);
	valReg->analogValue = valReg->analogValueI2CReadBuff.val;
	g_mutex_unlock(&valReg->valRegMutex);
}

static gpointer channelValRegSychornizeTask(gpointer data)
{
	TernixAnalogSensorChannel * channel = (TernixAnalogSensorChannel*)data;
	TernixAnalogSensorValueRegister* valReg = &channel->valRegVariables;

	printf("Channel: %d sensor value update task start\n", channel->id);

	while(isChannelEnabled(channel)) {
		g_mutex_lock(&valReg->updateMutex);
		while(valReg->isReadSucceed && !valReg->isSensorDataUpdated)
			g_cond_wait(&valReg->updateCond, &valReg->updateMutex);
		valReg->isSensorDataUpdated = FALSE;
		g_mutex_unlock(&valReg->updateMutex);

		if(I2cReadChannelVal(channel) != Success)
			printf("Add I2C session failed");

		g_thread_yield();
	}
	printf("Channel: %d sensor value update task end\n", ((TernixAnalogSensorChannel*)data)->id);
	g_thread_exit(data);

	return NULL;
}

void channelValRegI2cTaskCallBack(guchar i2cAddress, guchar i2cCMD, gpointer para, gint result)
{
	TernixAnalogSensorChannel * channel = (TernixAnalogSensorChannel*)para;

	printf("channel: %u, id: %u\n", channel, channel->id);

	switch(i2cCMD) {
	case CMD_ANALOG_SENSOR_DATA_READ:
		if(result == Success) {
			setChannelValRegUpdateFlag(channel, FALSE);
			refreshUpdatedValReg(channel);
			printf("Channel: %u, analog value updated: %u\n", channel->id, getChannelAnalogValue(channel));
		} else
			disableChannel(channel);
		break;
	default: break;
	}
}

// I2C TASK
ErrorCode I2cReadChannelVal(TernixAnalogSensorChannel * channel)
{
	TernixAnalogModuleInfoItem module = terNixAnalogModuleInfoTable[channel->moduleID - 1];

	printf("Channel: %u read Analog value\n", channel->id);

    return addNewI2cSession(channel->valRegVariables.callback, channel, module.i2cAddress, CMD_ANALOG_SENSOR_DATA_READ, I2C_TERNIX_ANANLOG_SENSOR_CHANNEL_MSG_LEN, I2C_TERNIX_ANANLOG_SENSOR_DATA_MSG_LEN, (guchar *)&channel->id, channel->valRegVariables.analogValueI2CReadBuff.bts);
}

ErrorCode I2cUpdateChannelConfig(TernixAnalogSensorChannel * channel)
{
	TernixAnalogModuleInfoItem module = terNixAnalogModuleInfoTable[channel->moduleID - 1];

    return addNewI2cSession(channel->conRegVariables.callback, (gpointer)channel, module.i2cAddress, CMD_ANALOG_CHANNEL_CONFIG, I2C_TERNIX_ANALOG_MODULE_INIT_CONFIG_MSG_LEN, I2C_TERNIX_NULL_MESSAGE_LEN, &channel->conRegVariables.conRegI2CWriteBuff, NULL);
}

// CHANNEL CONTROL
void startChannelTasks(TernixAnalogSensorChannel * channel)
{
	g_thread_new("channel sensor RD", channel->valRegVariables.valReadTask, channel);
	g_thread_new("channel configure WR", channel->conRegVariables.conWriteTask, channel);
}

void enableChannel(TernixAnalogSensorChannel * channel)
{
	if(isChannelEnabled(channel))
		return;

	setChannelConRegBit(channel, TERNIX_INPUT_ENABLED);
}

void disableChannel(TernixAnalogSensorChannel * channel)
{
	if(!isChannelEnabled(channel))
		return;

	clearChannelConRegBit(channel, TERNIX_INPUT_ENABLED);
}

gboolean isChannelEnabled(TernixAnalogSensorChannel * channel)
{
	gboolean isEnabled = FALSE;
	TernixAnalogSensorControlRegister * conReg = &channel->conRegVariables;

	g_mutex_lock(&conReg->conRegMutex);
	isEnabled = (conReg->RegByte.bits.enable == TERNIX_ENABLE_INPUT) ? TRUE : FALSE;
	g_mutex_unlock(&channel->conRegVariables.conRegMutex);

	return isEnabled;
}

void setSignalModeToCurrent(TernixAnalogSensorChannel * channel)
{
	clearChannelConRegBit(channel, TERNIX_SET_INPUT_VOLTAGE_MODE);
}

void setSignalModeToVoltage(TernixAnalogSensorChannel * channel)
{
	setChannelConRegBit(channel, TERNIX_SET_INPUT_VOLTAGE_MODE);
}

void setThreshold(TernixAnalogSensorChannel * channel, guchar val)
{
	TernixInputControlRegisterByte byt;

	byt.val = getChannelConRegVal(channel);

	if(byt.bits.threshold != val)
		byt.bits.threshold = val;

	setChannelConReg(channel, byt.val);
}

static void setChannelConRegBit(TernixAnalogSensorChannel * channel, guchar val)
{
	TernixAnalogSensorControlRegister * conReg = &channel->conRegVariables;

	g_mutex_lock(&conReg->conRegMutex);
	if(!(conReg->RegByte.val & val)) {
		conReg->RegByte.val |= val;
		conReg->conRegI2CWriteBuff = conReg->RegByte.val;
		setChannelConRegUpdateFlag(conReg, TRUE);
		g_cond_signal(&conReg->conRegCond);
	}
	g_mutex_unlock(&conReg->conRegMutex);
}

static void clearChannelConRegBit(TernixAnalogSensorChannel * channel, guchar val)
{
	TernixAnalogSensorControlRegister * conReg = &channel->conRegVariables;

	g_mutex_lock(&conReg->conRegMutex);
	if(conReg->RegByte.val & val) {
		conReg->RegByte.val &= (~val);
		conReg->conRegI2CWriteBuff = conReg->RegByte.val;
		setChannelConRegUpdateFlag(conReg, TRUE);
		g_cond_signal(&conReg->conRegCond);
	}
	g_mutex_unlock(&conReg->conRegMutex);
}
