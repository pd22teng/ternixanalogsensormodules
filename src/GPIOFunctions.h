#ifndef FUNCTIONGPIO_H_
#define FUNCTIONGPIO_H_

#include "AT91GPIO.h"
#include "glib.h"

#define __GLIB_SUPPORT__
 /****************************************************************
 * Constants
 ****************************************************************/

#define SYSFS_GPIO_DIR "/sys/class/gpio"
#define POLL_TIMEOUT (3 * 1000) /* 3 seconds */
#define MAX_BUF 64

typedef enum {
    INPUT_PIN = 0,
    OUTPUT_PIN = 1
} PinDirection;

typedef enum {
    LOW = 0,
    HIGH = 1
} PinValue;

typedef enum {
    NONE_EDGE   = 0,
    RISING_EDGE = 1,
    FALLING_EDGE = 2,
    BOTH_EDGE   = 3,
    EDGE_MODE_SIZE = 4
} EdgeMode;

int gpio_set_export(unsigned int gpio);
int gpio_set_unexport(unsigned int gpio);
int gpio_set_dir(unsigned int gpio, PinDirection out_flag);
int gpio_set_value(unsigned int gpio, PinValue value);
int gpio_get_value(unsigned int gpio, unsigned int *value);
int gpio_write_fd_value(int fd, unsigned int value);
int gpio_read_fd_value(int fd, unsigned int *value);
int gpio_set_edge(unsigned int gpio, EdgeMode mode);
int gpio_value_fd_open(unsigned int gpio);
int gpio_fd_close(int fd);

int gpio_set_input_mode(unsigned int gpio);
int gpio_set_output_mode(unsigned int gpio);

#ifdef __GLIB_SUPPORT__
int gpio_set_glib_event_handler(unsigned int gpio, GIOFunc eventHandler, gpointer user_data);
#endif

#endif /* FUNCTIONGPIO_H_ */
