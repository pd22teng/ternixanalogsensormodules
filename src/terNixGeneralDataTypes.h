/*
 * TernixGeneralDataTypes.h
 *
 *  Created on: Sep 9, 2015
 *      Author: teng
 */

#ifndef TERNIXGENERALDATATYPES_H_
#define TERNIXGENERALDATATYPES_H_

#include <sys/types.h>
#include <stdint.h>
#include <stddef.h>

#define NO  0
#define YES 1

#define CANCEL 0
#define FAILED 0
#define OK 1

typedef unsigned char uint8;
typedef unsigned short uint16;
typedef unsigned long uint32;
typedef unsigned long long uint64;

typedef signed char sint8;
typedef signed short sint16;
typedef signed long sint32;
typedef signed long long sint64;

typedef union {
	uint8 val;
	struct {
		unsigned b0 : 1;
		unsigned b1 : 1;
		unsigned b2 : 1;
		unsigned b3 : 1;
		unsigned b4 : 1;
		unsigned b5 : 1;
		unsigned b6 : 1;
		unsigned b7 : 1;
	} bits;
} uint8_val;

typedef union {
    uint16 val;

    uint8 bts[2];

    struct {
        uint8 lo;
        uint8 hi;
    } byte;
} uint16_val;

typedef union {
    uint32 val;

    uint8 bts[4];

    struct {
        uint16 lo;
        uint16 hi;
    } word;

    struct {
        uint8 lb;
        uint8 hb;
        uint8 ub;
        uint8 mb;
    } byte;
} uint32_val;

typedef union {
    uint64 val;

    struct{
       uint32 lo;
       uint32 hi;
    } DWord;

    struct {
        uint16 lw;
        uint16 hw;
        uint16 uw;
        uint16 mw;
    } word;

    uint8 bts[8];

    struct {
        uint8 b1;
        uint8 b2;
        uint8 b3;
        uint8 b4;
        uint8 b5;
        uint8 b6;
        uint8 b7;
        uint8 b8;
    } byte;
}uint64_val;

#define IS_BIT_SET_MASK(_FLAGS_, _MASK_)   (((_FLAGS_) & (_MASK_)) == (_MASK_))
#define IS_BIT_CLEAR_MASK(_FLAGS_, _MASK_) (((_FLAGS_) & (_MASK_)) == 0)

#define IS_BIT_SET_NUM(_FLAGS_, _NUM_)   (((_FLAGS_) & (1<<(_NUM_))) != 0)
#define IS_BIT_CLEAR_NUM(_FLAGS_, _NUM_) (((_FLAGS_) & (1<<(_NUM_))) == 0)

#define SET_BIT_NUM(_FLAGS_, _NUM_)   _FLAGS_ |=   1<<(_NUM_)
#define CLEAR_BIT_NUM(_FLAGS_, _NUM_) _FLAGS_ &= ~(1<<(_NUM_))

#define SET_BIT_MASK(_FLAGS_, _MASK_)   _FLAGS_ |=  (_MASK_)
#define CLEAR_BIT_MASK(_FLAGS_, _MASK_) _FLAGS_ &= ~(_MASK_)

#define IS_UINT32_BIT_SET_MASK(_FLAGS_, _MASK_)   ((((unsigned long)_FLAGS_) & ((unsigned long)_MASK_)) == ((unsigned long)_MASK_))
#define IS_UINT32_BIT_CLEAR_MASK(_FLAGS_, _MASK_) ((((unsigned long)_FLAGS_) & ((unsigned long)_MASK_)) == (unsigned long)0)

#define IS_UINT32_BIT_SET_NUM(_FLAGS_, _NUM_)   (((_FLAGS_) & ((unsigned long)1<<(_NUM_))) != (unsigned long)0)
#define IS_UINT32_BIT_CLEAR_NUM(_FLAGS_, _NUM_) (((_FLAGS_) & ((unsigned long)1<<(_NUM_))) == (unsigned long)0)

#define SET_UINT32_BIT_NUM(_FLAGS_, _NUM_)   _FLAGS_ |=   (unsigned long)1<<(_NUM_)
#define CLEAR_UINT32_BIT_NUM(_FLAGS_, _NUM_) _FLAGS_ &= ~((unsigned long)1<<(_NUM_))

#define SET_UINT32_BIT_MASK(_FLAGS_, _MASK_)   _FLAGS_ |=  ((unsigned long)_MASK_)
#define CLEAR_UINT32_BIT_MASK(_FLAGS_, _MASK_) _FLAGS_ &= ~((unsigned long)_MASK_)

#define IS_UINT64_BIT_SET_MASK(_FLAGS_, _MASK_)   ((((unsigned long long)_FLAGS_) & ((unsigned long long)_MASK_)) == ((unsigned long long)_MASK_))
#define IS_UINT64_BIT_CLEAR_MASK(_FLAGS_, _MASK_) ((((unsigned long long)_FLAGS_) & ((unsigned long long)_MASK_)) == (unsigned long long)0)

#define IS_UINT64_BIT_SET_NUM(_FLAGS_, _NUM_)   (((_FLAGS_) & ((unsigned long long)1<<(_NUM_))) != (unsigned long long)0)
#define IS_UINT64_BIT_CLEAR_NUM(_FLAGS_, _NUM_) (((_FLAGS_) & ((unsigned long long)1<<(_NUM_))) == (unsigned long long)0)

#define SET_UINT64_BIT_NUM(_FLAGS_, _NUM_)   _FLAGS_ |=   (unsigned long long)1<<(_NUM_)
#define CLEAR_UINT64_BIT_NUM(_FLAGS_, _NUM_) _FLAGS_ &= ~((unsigned long long)1<<(_NUM_))

#define SET_UINT64_BIT_MASK(_FLAGS_, _MASK_)   _FLAGS_ |=  ((unsigned long long)_MASK_)
#define CLEAR_UINT64_BIT_MASK(_FLAGS_, _MASK_) _FLAGS_ &= ~((unsigned long long)_MASK_)

#endif /* TERNIXGENERALDATATYPES_H_ */
