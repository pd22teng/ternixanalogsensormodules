/*
 * terNixANalogModulesTable.c
 *
 *  Created on: Sep 4, 2015
 *      Author: teng
 */

#include "terNixAnalogModulesTable.h"
#include "terNixInputChannelDataTypes.h"
#include "terNixI2cTask.h"

TernixInputID analog_module_one_input_channel_list[TERNIX_SNESOR_MODULE_ONE_CH_NUM] =
	{TERNIX_INPUT_ID_1,  TERNIX_INPUT_ID_2,  TERNIX_INPUT_ID_3,  TERNIX_INPUT_ID_4,  TERNIX_INPUT_ID_5,  TERNIX_INPUT_ID_6};

TernixInputID analog_module_two_input_channel_list[TERNIX_SNESOR_MODULE_TWO_CH_NUM] =
	{TERNIX_INPUT_ID_7,  TERNIX_INPUT_ID_8,  TERNIX_INPUT_ID_9,  TERNIX_INPUT_ID_10, TERNIX_INPUT_ID_11};

TernixInputID analog_module_thr_input_channel_list[TERNIX_SNESOR_MODULE_THR_CH_NUM] =
	{TERNIX_INPUT_ID_12, TERNIX_INPUT_ID_13, TERNIX_INPUT_ID_14, TERNIX_INPUT_ID_15, TERNIX_INPUT_ID_16, TERNIX_INPUT_ID_17};

TernixInputID analog_module_fou_input_channel_list[TERNIX_SNESOR_MODULE_FOU_CH_NUM] =
	{TERNIX_INPUT_ID_18, TERNIX_INPUT_ID_19, TERNIX_INPUT_ID_20, TERNIX_INPUT_ID_21, TERNIX_INPUT_ID_22};

TernixInputID analog_module_fiv_input_channel_list[TERNIX_SNESOR_MODULE_FIV_CH_NUM] =
	{TERNIX_INPUT_ID_23, TERNIX_INPUT_ID_24, TERNIX_INPUT_ID_25, TERNIX_INPUT_ID_26, TERNIX_INPUT_ID_27};

TernixInputID analog_module_six_input_channel_list[TERNIX_SNESOR_MODULE_SIX_CH_NUM] =
	{TERNIX_INPUT_ID_28, TERNIX_INPUT_ID_29, TERNIX_INPUT_ID_30, TERNIX_INPUT_ID_31, TERNIX_INPUT_ID_32};

const TernixAnalogModuleInfoItem terNixAnalogModuleInfoTable[TERNIX_ANALOG_MODULE_NUM] =
{
    {TERNIX_ANALOG_MODULE_ONE, ANALOG_MODULE_ONE_I2C_ADD, TERNIX_SNESOR_MODULE_ONE_CH_NUM, analog_module_one_input_channel_list},
    {TERNIX_ANALOG_MODULE_TWO, ANALOG_MODULE_TWO_I2C_ADD, TERNIX_SNESOR_MODULE_TWO_CH_NUM, analog_module_two_input_channel_list},
    {TERNIX_ANALOG_MODULE_THR, ANALOG_MODULE_THR_I2C_ADD, TERNIX_SNESOR_MODULE_THR_CH_NUM, analog_module_thr_input_channel_list},
    {TERNIX_ANALOG_MODULE_FOU, ANALOG_MODULE_FOU_I2C_ADD, TERNIX_SNESOR_MODULE_FOU_CH_NUM, analog_module_fou_input_channel_list},
    {TERNIX_ANALOG_MODULE_FIV, ANALOG_MODULE_FIV_I2C_ADD, TERNIX_SNESOR_MODULE_FIV_CH_NUM, analog_module_fiv_input_channel_list},
    {TERNIX_ANALOG_MODULE_SIX, ANALOG_MODULE_SIX_I2C_ADD, TERNIX_SNESOR_MODULE_SIX_CH_NUM, analog_module_six_input_channel_list}
};

guchar getChannelIndexFromChannelID(guchar channelId)
{
	guchar channelNum;
	TernixInputID * channels;
	guchar i,  j;

	guchar start = 0;
	guchar end = TERNIX_ANALOG_MODULE_NUM - 1;

	while(TRUE) {
		if(end < start)
			break;

		i = start + end;

		channelNum = terNixAnalogModuleInfoTable[i].channelNum;
		channels = terNixAnalogModuleInfoTable[i].inputChannels;

		if(channelId < channels[0]) {
			end = i - 1;
			continue;
		} else if(channelId > channels[channelNum - 1]) {
			start = i + 1;
			continue;
		}
		else
		{
			for(j = 0 ; j < channelNum; j++)
			{
				if(channelId == channels[j])
					return j+1;
			}
		}
	}
	return TERNIX_INPUT_NULL;
}

guchar getModuleIDFromChannelID(guchar channelId)
{
	guchar channelNum;
	TernixInputID * channels;
	guchar i,  j;

	guchar start = 0;
	guchar end = TERNIX_ANALOG_MODULE_NUM - 1;

	while(TRUE) {
		if(end < start)
			break;

		i = start + end;

		channelNum = terNixAnalogModuleInfoTable[i].channelNum;
		channels = terNixAnalogModuleInfoTable[i].inputChannels;

		if(channelId < channels[0]) {
			end = i - 1;
			continue;
		} else if(channelId > channels[channelNum - 1]) {
			start = i + 1;
			continue;
		}
		else
		{
			return terNixAnalogModuleInfoTable[i].id;
		}
	}
	return TERNIX_ANALOG_MODULE_NULL;
}

guchar getModuleIDFromI2cAddress(guchar i2cAddress)
{
	guchar id;
	guchar i;

	for(i = 0; i < TERNIX_ANALOG_MODULE_NUM; i++) {
		if(i2cAddress == terNixAnalogModuleInfoTable[i].i2cAddress) {
			id = terNixAnalogModuleInfoTable[i].id;
			break;
		}
	}
	return id;
}
