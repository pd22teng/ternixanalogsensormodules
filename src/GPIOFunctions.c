#include "GPIOFunctions.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
#include <poll.h>

const char none_edge_str[]    = "none";
const char rising_edge_str[]  = "rising";
const char falling_edeg_str[] = "falling";
const char both_edge_str[]    = "both";

const char* gpio_edge_mode[EDGE_MODE_SIZE] = {
    none_edge_str,
    rising_edge_str,
    falling_edeg_str,
    both_edge_str
};

/****************************************************************
 * gpio_export
 ****************************************************************/
int gpio_set_export(unsigned int gpio)
{
	int fd, len;
	char buf[MAX_BUF];

	fd = open(SYSFS_GPIO_DIR "/export", O_WRONLY);
	if (fd < 0) {
		perror("gpio/export");
		return fd;
	}

	len = snprintf(buf, sizeof(buf), "%d", gpio);
	write(fd, buf, len);
	close(fd);

	return 0;
}

/****************************************************************
 * gpio_unexport
 ****************************************************************/
int gpio_set_unexport(unsigned int gpio)
{
	int fd, len;
	char buf[MAX_BUF];

	fd = open(SYSFS_GPIO_DIR "/unexport", O_WRONLY);
	if (fd < 0) {
		perror("gpio/export");
		return fd;
	}

	len = snprintf(buf, sizeof(buf), "%d", gpio);
	write(fd, buf, len);
	close(fd);

	return 0;
}

/****************************************************************
 * gpio_set_dir
 ****************************************************************/
int gpio_set_dir(unsigned int gpio, PinDirection out_flag)
{
	int fd;
	char buf[MAX_BUF];

#ifndef _SAMA_DEVELOP_BOARD_
    snprintf(buf, sizeof(buf), SYSFS_GPIO_DIR  "/pio%s/direction", at91_gpio_str[gpio]);
#else
    snprintf(buf, sizeof(buf), SYSFS_GPIO_DIR  "/gpio%d/direction", gpio);
#endif
	fd = open(buf, O_WRONLY);
	if (fd < 0) {
        perror(&buf[0]);
		return fd;
	}

	if (out_flag == OUTPUT_PIN)
		write(fd, "out", 4);
	else
		write(fd, "in", 3);

	close(fd);
	return 0;
}

/****************************************************************
 * gpio_set_value
 ****************************************************************/
int gpio_set_value(unsigned int gpio, PinValue value)
{
	int fd;
	char buf[MAX_BUF];

#ifndef _SAMA_DEVELOP_BOARD_
    snprintf(buf, sizeof(buf), SYSFS_GPIO_DIR "/pio%s/value", at91_gpio_str[gpio]);
#else
    snprintf(buf, sizeof(buf), SYSFS_GPIO_DIR  "/gpio%d/value", gpio);
#endif

	fd = open(buf, O_WRONLY);
	if (fd < 0) {
        perror(&buf[0]);
		return fd;
	}

	if (value==LOW)
		write(fd, "0", 2);
	else
		write(fd, "1", 2);

	close(fd);
	return 0;
}

/****************************************************************
 * gpio_get_value
 ****************************************************************/
int gpio_get_value(unsigned int gpio, unsigned int *value)
{
	int fd;
	char buf[MAX_BUF];
	char ch;

#ifndef _SAMA_DEVELOP_BOARD_
    snprintf(buf, sizeof(buf), SYSFS_GPIO_DIR "/pio%s/value", at91_gpio_str[gpio]);
#else
    snprintf(buf, sizeof(buf), SYSFS_GPIO_DIR  "/gpio%d/value", gpio);
#endif

	fd = open(buf, O_RDONLY);
	if (fd < 0) {
        perror(&buf[0]);
		return fd;
	}

	read(fd, &ch, 1);

	if (ch != '0') {
        *value = 1;
	} else {
        *value = 0;
	}

	close(fd);
	return 0;
}


/****************************************************************
 * gpio_set_edge
 ****************************************************************/

int gpio_set_edge(unsigned int gpio, EdgeMode mode)
{
	int fd;
	char buf[MAX_BUF];

    if(mode >= EDGE_MODE_SIZE)
        return -1;

#ifndef _SAMA_DEVELOP_BOARD_
    snprintf(buf, sizeof(buf), SYSFS_GPIO_DIR "/pio%s/edge", at91_gpio_str[gpio]);
#else
    snprintf(buf, sizeof(buf), SYSFS_GPIO_DIR  "/gpio%d/edge", gpio);
#endif

	fd = open(buf, O_WRONLY);
	if (fd < 0) {
        perror(&buf[0]);
		return fd;
	}

    write(fd, gpio_edge_mode[mode], strlen(gpio_edge_mode[mode]));
	close(fd);
	return 0;
}

/****************************************************************
 * gpio_value_fd_open
 ****************************************************************/

int gpio_value_fd_open(unsigned int gpio)
{
	int fd;
	char buf[MAX_BUF];

#ifndef _SAMA_DEVELOP_BOARD_
    snprintf(buf, sizeof(buf), SYSFS_GPIO_DIR "/pio%s/value", at91_gpio_str[gpio]);
#else
    snprintf(buf, sizeof(buf), SYSFS_GPIO_DIR  "/gpio%d/value", gpio);
#endif

	fd = open(buf, O_RDONLY | O_NONBLOCK );
	if (fd < 0) {
        perror(&buf[0]);
	}
	return fd;
}

/****************************************************************
 * gpio_fd_close
 ****************************************************************/

int gpio_fd_close(int fd)
{
	return close(fd);
}


int gpio_read_fd_value(int fd, unsigned int *value)
{
    char ch;
    int tmp = 0;

    if (fd < 0) {
        perror("fd error!");
        return fd;
    }
    lseek(fd, 0, SEEK_SET);
    read(fd, &ch, 1);

    if(ch == '0')
        tmp = LOW;
    else
        tmp = HIGH;

    if(value != NULL)
        *value = tmp;

//    close(fd);
    return tmp;
}

int gpio_write_fd_value(int fd, unsigned int value)
{
    if (fd < 0) {
        perror("fd error!");
        return fd;
    }

    if (value == LOW)
        write(fd, "0", 2);
    else
        write(fd, "1", 2);

//    close(fd);
    return 0;
}

int gpio_set_input_mode(unsigned int gpio)
{
    int result = 0;

    // first export gpio pin
    if((result = gpio_set_export(gpio)) < 0)
        return result;

    // set direction as input
    result = gpio_set_dir(gpio, INPUT_PIN);

    return result;
}

int gpio_set_output_mode(unsigned int gpio)
{
    int result = 0;

    // first export gpio pin
    if((result = gpio_set_export(gpio)) < 0)
        return result;

    // set direction as input
    result = gpio_set_dir(gpio, OUTPUT_PIN);

    result = gpio_set_value(gpio, LOW);

    return result;
}

#ifdef __GLIB_SUPPORT__
// return fd if succeed
int gpio_set_glib_event_handler(unsigned int gpio, GIOFunc eventHandler, gpointer user_data)
{
    gint fd = gpio_value_fd_open(gpio);

    if(fd < 0)
        return -1;

    GIOChannel* channel = g_io_channel_unix_new(fd);
    GIOCondition condition = G_IO_PRI;
    g_io_add_watch(channel, condition, eventHandler, user_data);

    return fd;
}
#endif
